variable "token" {}
 provider "vscale" {
   token = "${var.token}"
 }
 provider "aws" {
  region     = "us-west-2"
  shared_credentials_file = "~/.aws/credentials"}

resource "vscale_ssh_key" "web" {
 "name" = "key1"
 "key" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDta/k5yHXycYOFRVDHznDtgdJMZp9fkYTzJqG9JJAM1NbLAj3blXWn95+RQPKA683VpTUaB7va1Ed45crlTq9hfZWqujNZQ14yYaGWrUbqyrIa420lNFN1xTptEpvCEPnIKKni+d4K5csY2a9Xl+gdFuClCYUUEn3GAALE4BemD3eh1Ii1aUYwCr0/SKD310PxEPYsJFZazOjELS6JNej5cF/VlkIWXriD83Gj5eM1tpvYzsL45K8r9zCJXhMtD1OMIkd8J9v6KOlOoythwN7GWn9PIq52W2pNcK4CCAjYztISKanovCY/El5AUjy4QwG99NlVIfbewkFJ/i3z+tnCwqpJklNSnt6GDw2tAmakUii6zo9KDftgQ2QvcnwmbIQupxm3PNuM+Ij9ZGCw5PcsXK5EWOc8Aq/Beq7gI4MHC2luuPjOk3AoP5CtjxA/eGC3VRRHdfBwEaGCTvsZ2Zvhk/SFzFd5COBpFuEPJs/FXP8cWtMhmRD64yXRO6m0XeNjX2H+bawrD8jvm5eOdl42FL6YX/PrSzlvHAmWmMIktW3Vru1ceZSFcVU13ChVQZPhXP0Gsu2gzx6UCjcMCaPLZiO51pfsN4RzDxAL2AmNJCyn7VqCuvyUQZc3Skey6K7D+P7c5ycczR9jrYmT5fifhmHebD6vBjbG9dAZwzc/Mw== your_email@example.com"
 }

variable "name" {
 type = "list"
 default = ["nvestan-1","nvestan-2"]
}


resource "vscale_scalet" "web" {
  make_from = "ubuntu_16.04_64_001_master"
  rplan = "medium"
 count = "${length(var.name)}"
 name = "nvestan-${(count.index)}"
 location = "msk0"
  ssh_keys = ["${vscale_ssh_key.web.id}"]  }

#output "address" {
#  value = "${vscale_scalet.web.*.public_address}"
#}

  resource "aws_route53_record" "www" {
 count = 2
 zone_id = "ZCOKAAJ9UMS0T"
 name    = "nvestan-${count.index}.devops.rebrain.srwx.net"
  type    = "A"
  ttl     = "300"
  records = ["${element(vscale_scalet.web.*.public_address, count.index)}"]
}

